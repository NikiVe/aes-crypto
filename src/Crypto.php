<?php

namespace SOME\Crypto;

class Crypto {
    protected string $user = 'USER';
    protected string $password = 'FIMIProd';
    protected string $challenge = '64C89F99C6A988403D57004A71D947CA';

    public function __construct($user, $password, $challenge) {
        $this->user = $user;
        $this->password = $password;
        $this->challenge = $challenge;
    }

    public function getPasswordHash() {
        return hash('sha3-256', strtoupper($this->user) . $this->password, true);
    }

    public function getChallengeByHashPassword(): string {
        return openssl_encrypt(
            $this->challenge,
            'aes-256-ecb',
            $this->getPasswordHash(),
            OPENSSL_RAW_DATA
        );
    }
}