override container-name-app = app
override docker-compose = docker-compose -f docker-compose.yml
override docker-run = $(docker-compose) run -T $(container-name-app)
override php-composer = $(docker-run) composer

# Docker
up: FORCE ; $(docker-compose) up -d --remove-orphans --no-build
build-compress: FORCE ; $(docker-compose) build --pull --compress --no-rm
build: FORCE ; $(docker-compose) build --pull --no-rm
pull: FORCE ; $(docker-compose) pull
push: FORCE ; $(docker-compose) push

# APP
sh: FORCE ; $(docker-run) sh
run: FORCE ; $(docker-run) php app.php

update: FORCE ; $(php-composer) update
test: FORCE ; $(php-composer) test

# Cheat

FORCE:
