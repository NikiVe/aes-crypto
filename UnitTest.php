<?php

namespace ABR\TWPGFIMI\Test\Unit\Security;

use SOME\Crypto\Crypto;

class UnitTest extends \PHPUnit\Framework\TestCase
{
    public function testPasswordHash() {
        $s = new Crypto('USER', 'FIMIProd', '64C89F99C6A988403D57004A71D947CA');
        self::assertEquals(
            'E4ADD5A841C7214FF0C736EC8344D62839FE3BB18E526717EB8B2D7D287A14C1',
            strtoupper(bin2hex($s->getPasswordHash()))
        );

        self::assertEquals(
            '210589AC6D0C671DEA056A4B35F54F0165677F7EE84EBF59BAC5618711190B01',
            bin2hex($s->getChallengeByHashPassword()),
        );
    }
}
