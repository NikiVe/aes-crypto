ARG PHP_BASEIMAGE_VERSION
ARG DOCKER_COMMON_IMAGES

FROM mlocati/php-extension-installer as local-php-extension-installer

FROM php:7.4-cli-alpine as base
COPY --from=local-php-extension-installer /usr/bin/install-php-extensions /usr/bin/

RUN apk update && apk add git composer
RUN install-php-extensions sockets pcntl
ENV GIT_SSL_NO_VERIFY=true
RUN cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY ./php/app.ini $PHP_INI_DIR/conf.d/90-app.ini
WORKDIR /app/

FROM base as develop
COPY . /app